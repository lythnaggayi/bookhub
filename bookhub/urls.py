from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "", TemplateView.as_view(template_name="base_generic.html"), name="base_generic"
    ),
    path("accounts/", include("django.contrib.auth.urls")),
    path("accounts/login/", include("django.contrib.auth.urls")),
    path("accounts/logout/", include("django.contrib.auth.urls")),
    path("users", include("interfaces.urls")),
]
