# Generated by Django 4.0.6 on 2022-08-17 19:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interfaces', '0015_alter_bookcopy_borrow_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookcopy',
            name='borrow_date',
            field=models.DateField(blank=True, default=datetime.datetime(2022, 8, 17, 22, 6, 8, 213210), null=True),
        ),
        migrations.AlterField(
            model_name='bookcopy',
            name='due_back',
            field=models.DateField(auto_now=True, null=True),
        ),
    ]
