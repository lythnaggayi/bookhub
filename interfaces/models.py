from django.db import models
from django.urls import reverse
import uuid
from django.contrib.auth.models import User
import datetime


class Genre(models.Model):
    """A model to represent the different book genres"""

    name = models.TextField(max_length=50, help_text="Enter a genre.")

    def __str__(self):
        return self.name


class Book(models.Model):
    """This represents the books that will be in the library"""

    title = models.CharField(max_length=200)
    author = models.ForeignKey("Author", on_delete=models.SET_NULL, null=True)
    blurb = models.TextField(
        max_length=2000, help_text="Enter a brief summary of the book."
    )
    genre = models.ManyToManyField(
        "Genre",
        help_text="Select a genre for this book",
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("book-detail", args=[str(self.id)])

    def display_genre(self):
        """Return a string for the genre so it can display it on the admin site"""
        return ", ".join(genre.name for genre in self.genre.all()[:3])

    display_genre.short_description = "Genre"


class Author(models.Model):
    """A model for the authors of the books in the library"""

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    class Meta:
        ordering = ["first_name", "last_name"]

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_absolute_url(self):
        return reverse("author-detail", args=[str(self.id)])


# def due_back_date():
#     # Users will have to return the book after 5 days
#     return datetime.datetime.today() + datetime.timedelta(days=5)


class BookCopy(models.Model):
    """A model for specific copies of books that will be borrowed from the library."""

    copy_num = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        help_text="The unique identification for this book in the whole library.",
    )
    book = models.ForeignKey("Book", on_delete=models.RESTRICT, null=True)
    imprint = models.CharField(
        max_length=100, help_text="Enter the name(alias) of the publishing company"
    )
    borrow_date = models.DateField(null=True, blank=True)
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    due_back = models.DateField(null=True, blank=True)

    possible_statuses = (
        ("r", "RESERVED"),
        ("a", "AVAILABLE"),
        ("b", "BORROWED"),
        ("w", "BEING WORKED ON"),
    )
    status = models.CharField(
        max_length=1,
        choices=possible_statuses,
        blank=True,
        null=True,
        default="a",
        help_text="Availability status",
    )

    def fine(self):
        if self.status == "b":
            days_overdue = 0
            days_overdue = datetime.date.today() - self.due_back

            days_overdue = days_overdue.days
            if days_overdue <= 0:
                return 0
            elif days_overdue <= 3:
                return 3000
            elif days_overdue > 3:
                return 10000

    def __str__(self):
        return f"{self.copy_num} ({self.book.title})"

    class Meta:
        verbose_name_plural = "Book Copies"
        permissions = (("can_mark_as_returned", "Can set book as returned"),)
