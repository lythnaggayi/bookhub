from django.urls import path
from . import views


urlpatterns = [
    path("hub/", views.BookListView.as_view(), name="hub"),
    path("hub/book/<int:pk>/", views.BookDetailView.as_view(), name="book-detail"),
    path("mybooks/", views.UserBorrowedBooksView.as_view(), name="user-books"),
    path("all-borrowed/", views.AllBorrowedBooksView.as_view(), name="all-borrowed"),
    path("hub/book/<uuid:copy_num>/borrow", views.BorrowView, name="borrow"),
    path("book/<uuid:copy_num>/return", views.ReturnView, name="return"),
    # path("book/<uuid:copy_num>/reserve", views.ReturnView, name="reserve"),
    # path("book/<uuid:copy_num>/maintain", views.ReturnView, name="maintain"),
]
