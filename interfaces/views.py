from django.shortcuts import render, get_object_or_404
from django.views import generic
from .models import Book, BookCopy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime


class BookListView(generic.ListView):
    model = Book
    context_object_name = "book_list"
    template_name = "interfaces/hub.html"
    ordering = ["title"]
    # paginate_by = 10


class BookDetailView(generic.DetailView):
    model = Book


class UserBorrowedBooksView(LoginRequiredMixin, generic.ListView):
    """This will let users see books they have borrowed"""

    model = BookCopy
    template_name = "interfaces/user-books.html"
    context_object_name = "borrowed_list"

    def get_queryset(self):
        return (
            BookCopy.objects.filter(borrower=self.request.user)
            .filter(status__exact="b")
            .order_by("due_back")
        )


class AllBorrowedBooksView(PermissionRequiredMixin, generic.ListView):
    """This is for borrowed books that will be seen by the librarian"""

    model = BookCopy
    template_name = "interfaces/all-borrowed.html"
    context_object_name = "borrowed_list"
    permission_required = "interfaces.can_mark_as_returned"

    def get_queryset(self):
        return BookCopy.objects.filter(status__exact="b").order_by("due_back")


def BorrowView(request, copy_num):
    """For borrowing books"""
    book_copy_borrow = get_object_or_404(BookCopy, pk=copy_num)
    book_copy_borrow.borrow_date = datetime.date.today()
    # book_copy_borrow.due_back = datetime.date.strftime(due_back_date, "%d/%m/%y")
    book_copy_borrow.due_back = book_copy_borrow.borrow_date + datetime.timedelta(
        days=5
    )
    book_copy_borrow.borrower = request.user
    book_copy_borrow.status = "b"
    book_copy_borrow.save()
    return HttpResponseRedirect(reverse("user-books"))


def ReturnView(request, copy_num):
    """For returning borrowed books"""
    book_copy_return = get_object_or_404(BookCopy, pk=copy_num)
    book_copy_return.borrow_date = None
    book_copy_return.due_back = None
    book_copy_return.borrower = None
    book_copy_return.status = "a"
    book_copy_return.save()
    return render(request, "interfaces/all-borrowed.html", {"copy": book_copy_return})


# def ReserveView(request, copy_num):
#     book_copy_reserve = get_object_or_404(BookCopy, pk=copy_num)
#     book_copy_reserve.borrow_date = None
#     book_copy_reserve.borrower = None
#     book_copy_reserve.status = "r"
#     book_copy_reserve.save()
#     return render(request, "interfaces/all-borrowed.html", {"copy": book_copy_return})


# def WorkingView(request, copy_num):
#     book_copy_working = get_object_or_404(BookCopy, pk=copy_num)
#     book_copy_working.borrow_date = None
#     book_copy_working.borrower = None
#     book_copy_working.status = "w"
#     book_copy_working.save()
#     return render(request, "interfaces/all-borrowed.html")
