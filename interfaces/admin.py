from django.contrib import admin
from .models import Genre, Author, Book, BookCopy


admin.site.register(Genre)


class BookAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "display_genre")


admin.site.register(Book, BookAdmin)


class AuthorAdmin(admin.ModelAdmin):
    """Display authors' names instead of default string"""

    list_display = ("first_name", "last_name")

    """Display fields to appear in the author form in order"""
    fields = ["first_name", "last_name"]


admin.site.register(Author, AuthorAdmin)


class BookCopyAdmin(admin.ModelAdmin):
    list_filter = ("status", "due_back")

    list_display = ("copy_num", "book", "status", "borrow_date", "due_back", "borrower")

    fieldsets = (
        (None, {"fields": ("book", "imprint", "copy_num")}),
        ("Availability", {"fields": ("status", "borrower", "borrow_date", "due_back")}),
    )


admin.site.register(BookCopy, BookCopyAdmin)
