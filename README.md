# BookHub 

BookHub is a library database management system. Typically designed for a school library, it allows a student to log into the system and search then request for a book. It also tracks the book from time of borrowing to time of return and shows the student the issued fine if the book is overdue.
